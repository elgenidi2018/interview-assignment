assignment (1) : React User Registration App
A multi-step user registration React application with form validation and submission to a mock authentication API.

Features

Step 1: Personal Information

Collects first name, last name, email, and date of birth.
Implements client-side validation for required fields, email format, and age (must be at least 18 years old).
Navigates to the next step only if the information is valid.


Step 2: Address Information

Collects address, city, state, and postal code.
Implements client-side validation for required fields.
Navigates to the next step only if the information is valid.


Step 3: Review and Submit

Displays a summary of the entered information from previous steps.
Provides a "Submit" button to send data to a mock authentication API.
Shows loading state while data is being submitted.
Displays a success message upon successful submission.
Handles errors and displays an error message if the submission fails.


Requirements

Built with React for the UI.
Efficient form state and validation management.
Asynchronous data fetching for submitting user data to the API.
React Router or any other routing library for multi-step form navigation.
State management (Redux, Recoil, etc.) for handling global state if necessary.
Responsive design for different screen sizes.
Unit tests for critical components and validation logic.


Bonus Points

Form persistence for users to continue where they left off.
Animations or transitions between form steps.
Enhanced accessibility for users with disabilities.
Secure storage of sensitive data (e.g., API keys) using environment variables.


Mock Authentication API
Use the following API endpoint for submitting user registration data:
POST /api/register

Submit the user data as JSON in the request body. Example User Object:
{ "firstName": "Ahmed", "lastName": "Elsayed", "email": "Ahmed.Elsayed@example.com", "dateOfBirth": "1990-01-01", "address": "123 mans St", "city": "Mansoura", "state": "EG", "postalCode": "12345" }

assignment (2) Redux Shopping Cart
A React application demonstrating state management with Redux. Users can add and remove items from a shopping cart.

Features

Utilizes Redux for state management.
Implements actions, reducers, and a store to manage the shopping cart state.
Displays a list of items with an "Add to Cart" button.
Allows users to add and remove items from the shopping cart.
Displays the current items in the shopping cart with the total price.


Requirements

Uses Redux for state management.
Creates actions, reducers, and a store for managing the shopping cart state.
Displays a list of items with an "Add to Cart" button.
Implements functionality to add and remove items from the shopping cart.
Displays the current items in the shopping cart with the total price.


assignment (3) Dynamic Data Visualization with React
A React application that fetches dynamic data from a public API and visualizes it using a charting library for an interactive and meaningful user experience.

Features

Fetches dynamic data from a public API.
Utilizes a charting library (e.g., Chart.js, D3.js) for creating interactive visualizations.
Implements features such as zooming, panning, or filtering for an enhanced user experience.
Ensures the application is responsive and handles large datasets efficiently.


Requirements

Fetches dynamic data from a public API.
Utilizes a charting library for interactive visualizations.
Implements features like zooming, panning, or filtering.
Ensures responsiveness and efficient handling of large datasets.


Share your solution on GitHub with detailed documentation. Include information on how to set up the project, handle data fetching, and customize the visualization components. Consider adding unit tests for critical functionalities.
